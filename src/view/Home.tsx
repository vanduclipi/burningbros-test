import {Col, Row, Spin} from "antd";
import {useInfiniteQuery} from "react-query";
import $apiFactory from "../services";
import CardProduct from "../components/CardProduct";
import {FC, useState} from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import {product} from "../type/product.type.ts";

const Home: FC = () => {
    const [listProduct, setListProduct] = useState<product[]>([])
    const {
        isSuccess,
        hasNextPage,
        fetchNextPage
    } = useInfiniteQuery(["getListProductsInfinity"],
        ({pageParam = 1}) => $apiFactory.productService.getListProduct(20, pageParam), {
            getNextPageParam: (lastPage) => {
                if (lastPage.limit + lastPage.skip >= lastPage.total) {
                    return undefined
                }
                return (lastPage.limit + lastPage.skip) / 20 + 1
            },
            onSuccess: (data) => {
                let formatData: product[] = [];
                data.pages.forEach((item) => {
                    formatData = [...formatData, ...item.products]
                })
                setListProduct(formatData)
            },
            keepPreviousData: true
        })

    return (
        <div className={"w-full max-w-[1400px] h-full mx-auto"}>
            <InfiniteScroll
                dataLength={listProduct.length}
                next={fetchNextPage}
                hasMore={hasNextPage ? hasNextPage : false}
                loader={ <Row>
                    <Col  span={12} offset={6}>
                        <Spin/>
                    </Col>
                </Row>}
                style={{
                    overflow :"hidden"
                }}
            >
                <Row gutter={[16, 24]}>
                    {
                        isSuccess && listProduct.length >= 0
                        && listProduct.map(product =>
                            <Col className="gutter-row" span={6} key={product.id}>
                                <CardProduct
                                    title={product.title}
                                    description={product.description}
                                    srcImage={product.images[product.images.length - 1]}
                                />
                            </Col>)
                    }
                </Row>

            </InfiniteScroll>
        </div>
    )
}
export default Home
