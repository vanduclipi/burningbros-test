import React from 'react'
import {Card} from "antd";
import "./style.css";
interface Props {
    srcImage : string;
    title :string;
    description :string;
    isLoading ?:boolean
}
const CardProduct:React.FC<Props> = ({srcImage, title, description, isLoading = false}) => {
    return (
        <Card
            loading={isLoading}
            hoverable
            cover={<div className="w-full h-[200px]">
                <img alt="product" className="object-cover w-full h-full" src={srcImage} />
            </div>}
        >
            <Card.Meta title={title} description={description} className="custom-card" />
        </Card>
    )
}
export default CardProduct
