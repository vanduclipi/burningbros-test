import React, {useEffect, useState} from 'react'
import {Input} from "antd";
import { useNavigate} from "react-router-dom";
import useDebounce from "../../core/hooks/useDebounce.ts";

const DEFAULT_DEBOUNCE = 500 // debounce value with 500ms
const Header:React.FC = () => {
    const navigation = useNavigate()
    const [search , setSearch] = useState<string>("")
    const debouncedQuery = useDebounce<string>(search, DEFAULT_DEBOUNCE);
    const handleChange = (e:React.ChangeEvent<HTMLInputElement>):void => {
        setSearch(e.target.value)
    }
    useEffect(() => {
        if(debouncedQuery === "" || debouncedQuery.trim() === ""){
            navigation(`/`)
            return;
        }
        navigation(`/search?q=${debouncedQuery}`)
    },[debouncedQuery])

    return (
        <div className="w-full max-w-[1400px] mx-auto shadow-gray-600 flex items-center justify-between ">
            <div className="logo w-[64px] h-[64px] mr-0">
                <img src="/vite.svg" className="w-full h-full object-cover mr-0" alt="logo"/>
            </div>
            <div className="search w-[300px]">
                <Input className="p-2" placeholder="Search your product...." value={search} onChange={handleChange}  />
            </div>
        </div>
    )
}
export default Header
