import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.tsx'
import './index.css'
import {queryClient} from "./plugins/react-query.ts";
import {QueryClientProvider} from "react-query";

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
    <React.StrictMode>
        <QueryClientProvider client={queryClient}>
                <App/>
        </QueryClientProvider>
    </React.StrictMode>,
)
