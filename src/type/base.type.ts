export interface metaData{
    skip: number;
    total: number;
    limit: number;
}