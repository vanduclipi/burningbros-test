import {metaData} from "./base.type.ts";

export  interface  product  {
    id: number;
    title: string;
    description: string;
    price: number;
    discountPercentage: number;
    rating: number;
    stock: number;
    brand: string;
    category: string;
    thumbnail: string;
    images: string[];
}

export interface listProduct extends metaData{
    products :product[],
}