import {Layout} from "antd";
import React from "react";
import {Outlet} from "react-router-dom";
import Header from "../components/Header";


const MainLayout: React.FC = () => {
    return <Layout className="w-full h-full">
        <Layout.Header className={"!bg-slate-100 shadow shadow-slate-300 py-2 h-full  mb-3.5 sticky top-0 z-10 "}>
            <Header />
        </Layout.Header>
        <Layout.Content className={"w-full "}>
            <Outlet/>
        </Layout.Content>
    </Layout>

}

export default MainLayout