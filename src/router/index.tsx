
import {
    createBrowserRouter,
} from "react-router-dom";
import Home from "../view/Home.tsx";
import MainLayout from "../layouts/MainLayout.tsx";
import Search from "../view/Search.tsx";


const router = createBrowserRouter([{
    path: "/",
    element: <MainLayout />,
    children: [{
        index: true,
        element :<Home />
    },{
        path: "search",
        element :<Search />
    }]
},
]);


export default  router