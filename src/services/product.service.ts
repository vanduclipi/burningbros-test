import BaseApiService from "./baseApi.service.ts";
import {PRODUCT, SEARCH_PRODUCT} from "./entities";
import {listProduct} from "../type/product.type.ts";

class ProductService extends BaseApiService {
    getListProduct( limit : number, page: number):Promise<listProduct>{
        console.log({page})
        const skip = limit*page -limit ;
        const params = {limit,skip}
        return this.get(PRODUCT,params)
    }
    getListSearchProducts(name:string,limit : number, page: number):Promise<listProduct>{
        console.log({page})
        const skip = limit*page -limit ;
        const params = {q:name,limit,skip}
        return this.get(SEARCH_PRODUCT,params)
    }
}
export default ProductService