/* eslint-disable no-unused-vars */

import type { AxiosInstance, AxiosResponse } from 'axios';
import axios from 'axios';

/* eslint-disable @typescript-eslint/no-explicit-any */
export interface $HttpClient {
    get<T = any>(path: string): Promise<T>;
    post<T = any>(path: string, body: any): Promise<T>;
    put<T = any>(path: string, body: any):  Promise<T>;
    patch<T = any>(path: string, body: any):  Promise<T>;
    delete<T = any>(path: string):  Promise<T>;
}

export default class BaseApiService implements $HttpClient {
    private $axiosInstance!: AxiosInstance;
    private readonly DEFAULT_TIMEOUT = 10000;
    private readonly BASE_URL = import.meta.env.VITE_BASE_API_URL || '';
    constructor() {
        this.$axiosInstance = this.initAxiosInstance();
    }
    /**
     * This method will config axios instance
     * If the request fails with a 401, then try to refresh the token and retry the request.
     *
     * @return The AxiosInstance is being returned.
     */
    private initAxiosInstance(): AxiosInstance {
        const axiosInstance = axios.create({
            baseURL: this.BASE_URL,
            withCredentials: false,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            timeout: this.DEFAULT_TIMEOUT,
        });
        axiosInstance.interceptors.request.use((request) => {
           // handle token here if have token
            return request;
        });
        axiosInstance.interceptors.response.use(
            (response: AxiosResponse): AxiosResponse => {
                if (response && response.data) {
                    return response.data;
                }
                return response;
            },
            async (error) => {
                //If the request fails with a 401, then try to refresh the token and retry the request.
                // because this is a test, so it has no config with token and refresh token
                return Promise.reject(error);
            },
        );
        return axiosInstance;
    }

    /**
     * This function returns a promise that resolves to an AxiosResponse object.
     * Get method
     * @param path The path to the resource you want to retrieve.
     * @param params {
     * @return The return type is a Promise of an AxiosResponse.
     */
    get<T = any>(path: string, params: any = {}): Promise<T> {
        return this.$axiosInstance.get(path, {
            params,
        });
    }
    /**
     * A function that returns a promise.
     *  Post method
     * @param path string - The path to the endpoint you want to hit.
     * @param body any
     * @return The return type is a Promise of an AxiosResponse.
     */

    post<T = any>(path: string, body: any): Promise<T> {
        return this.$axiosInstance.post(path, body);
    }

    /**
     * This function returns a promise that resolves to an AxiosResponse object that contains a generic
     * type T.
     *  Put method
     * @param path The path to the endpoint you want to hit.
     * @param body The body of the request.
     * @return The return type is a Promise of an AxiosResponse of type T.
     */
    put<T = any>(path: string, body: any):  Promise<T> {
        return this.$axiosInstance.put(path, body);
    }

    /**
     * This function returns a promise that resolves to an AxiosResponse object.
     * Patch method
     * @param path The path to the endpoint you want to hit.
     * @param body The body of the request.
     * @return The return type is a Promise of an AxiosResponse of type T.
     */
    patch<T = any>(path: string, body: any):  Promise<T> {
        return this.$axiosInstance.patch(path, body);
    }

    /**
     * This function returns a promise that resolves to an AxiosResponse object that contains a generic
     * type T.
     * Delete method
     * @param path The path to the resource you want to access.
     * @return The return type is a Promise of an AxiosResponse of type T.
     */
    delete<T = any>(path: string):  Promise<T> {
        return this.$axiosInstance.delete(path);
    }
}
