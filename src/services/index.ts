import ProductService from "./product.service.ts";

const $apiFactory = {
    productService : new ProductService()
}
export default $apiFactory